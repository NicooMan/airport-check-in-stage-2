package main;
import java.util.Arrays;
import java.util.Random;


public class Baggage {
	private double weight;
	private int [] dimension = new int [3];
	private float price;
	
	/**
	 * @param constructor with input
	 */
	public Baggage(double weight,int [] dimension) {

		this.weight=weight;
		this.dimension=dimension;
		this.price=0;
		
	}
	/**
	 * @param constructor without input
	 */
	public Baggage() {

		this.weight=0;
		this.dimension[0]= 0;
		this.dimension[1]= 0;
		this.dimension[2]= 0;
		
		this.price=0;
		
	}
	/**
	 * @param set the weight
	 */
	public void setWeight(double weight) {
		try {
			if (weight > 0) {
				this.weight=weight;
			}
			else {
				throw new Exception("You can enter only a positive weight! ");
			}
		}
		catch (Exception ex){
			System.out.println(ex.getMessage());
		}

		
	}
	/**
	 * Set a random baggage
	 */
	public void setBagageRandom() {
		this.weight=new Random().nextInt(50);
		this.dimension[0]= new Random().nextInt(100);
		this.dimension[1]= new Random().nextInt(100);
		this.dimension[2]= new Random().nextInt(100);
	}
	/**
	 * @param set dimensions
	 */
	public void setDimension(int [] dimension) {
		try {
			if (dimension.length != 3) {
				throw new Exception("You can enter only 3 dimensions! ");
			}
			else {
				if ((dimension[0]>0) && (dimension[1]>0) && (dimension[2]>0 )) {
					this.dimension=dimension;
				}
				else {
					throw new Exception("You can enter only positives dimensions! ");
				}
				
			}
		}
		catch (Exception ex){
			System.out.println(ex.getMessage());
		}
		
	}
	/**
	 * @param set price
	 */
	public void setPrice(float price) {
		try {
			if (price > 0) {
				this.price=price;
			}
			else {
				throw new Exception("You can enter only a positive price! ");
			}
		}
		catch (Exception ex){
			System.out.println(ex.getMessage());
		}
		
	}
	/**
	 * @param calculate and set price
	 */
	public float calculatePrice() {
		// Air France computation method
		
		int sum = this.dimension[0]+this.dimension[1]+this.dimension[2];
		this.price=0;
		
		if (sum<=158) {
			if(this.weight<=23) {
				return this.price;
			}
			else {
				this.price=this.price+70;
				return this.price;
			}
		}
		else {
			this.price=this.price+75;
			return this.price;
		}
	}
	/**
	 * @return a calculate volume
	 */
	public int getVolume() {
		return dimension[0] * dimension[1] * dimension[2];
	}
	/**
	 * @return the weight
	 */
	public double getWeight() {
		return this.weight;
	}
	/**
	 * @return dimensions
	 */
	public int [] getDimension() {
		return this.dimension;
	}
	/**
	 * @return the price
	 */
	public float getPrice() {
		return this.price;
	}

}
    