package main;

import java.util.HashMap;

public class Airport {
	private Queue queue;
	private Desk[] desks;
	private HashMap<String, Flight> flightMap;
	
	public Airport(HashMap<String, Flight> flightMap) {
		this.flightMap = flightMap;
		queue = new Queue();
		desks = new Desk[5];
		for (int i = 0; i < 5; i++) {
			desks[i] = new Desk(i, flightMap, queue);
			desks[i].open();
		}
	}
	
	public Queue getQueue() {
		return queue;
	}
	
	public Desk[] getDesks() {
		return desks;
	}
	
	public HashMap<String, Flight> getFlightMap() {
		return flightMap;
	}
	
	public void start() {
		for (int i = 0; i < 5; i++) {
			desks[i].start();
		}
	}
}
