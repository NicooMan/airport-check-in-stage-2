package main;

import java.util.ArrayList;
import java.util.HashMap;

//Class handling the interactions between the model and the view
public class Controller {
	private Airport model;
	private GUI view;
	private Booking[] lastPassenger;
	private HashMap <String, String> lastFlightInfo;

	public Controller(GUI view, Airport model) {	
		this.view = view;
		this.model = model;
		this.lastPassenger = new Booking[this.model.getDesks().length];
		for (int i = 0; i < model.getDesks().length; i++) {
			lastPassenger[i] = null;
		}
		this.lastFlightInfo = new HashMap<String, String>();
		for (Flight flight : model.getFlightMap().values()) {
			lastFlightInfo.put(flight.getFlightNumber(), flight.getFlightInfo());
		}
	}
	
	public void sendToDesk(int deskNumber,Booking booking) {
		Desk desk = model.getDesks()[deskNumber];
		desk.setCurrentBooking(booking);
	}
	
	public void openDesk(int deskNumber) {
		Desk desk = model.getDesks()[deskNumber];
		desk.open();
	}
	
	public void closeDesk(int deskNumber) {
		Desk desk = model.getDesks()[deskNumber];
		desk.close();
		view.setDeskText(desk.getNb(), "The desk is closed.");
	}	
	
	//Checks the status of every desk in the GUI and updates the model (open or close deks) accordingly
	public void checkDesk() {
		boolean[] buttonBoolean = view.buttonBoolean();
		
		for (int i=0;i<buttonBoolean.length;i++) {
			if (buttonBoolean[i] == true) {
				openDesk(i);
			}
			else {
				closeDesk(i);
			}
		}
	}
	
	//Updates the current passengers details displayed on the desks
	public void checkCurrentPassengers() {
		Desk[] desks = model.getDesks();
		for (int i = 0; i < desks.length; i++) {
			if (desks[i].getCurrentBooking() != lastPassenger[i]) {
				if (desks[i].getCurrentBooking() != null) {
					view.setDeskText(desks[i].getNb(), desks[i].getCurrentBooking().getBookingInfo());
					view.removeFirst();
					lastPassenger[i] = desks[i].getCurrentBooking();
				} else {
					view.setDeskText(desks[i].getNb(), "The desk is available.");
					lastPassenger[i] = null;
				}
			}
		}
	}
	
	//Updates the flights info displayed
	public void checkFlights() {
		for (Flight flight : model.getFlightMap().values()) {
			if (lastFlightInfo.get(flight.getFlightNumber()) != flight.getFlightInfo()) {
				view.setPlaneText(flight.getFlightNumber(), flight.getFlightInfo());
				lastFlightInfo.put(flight.getFlightNumber(), flight.getFlightInfo());
			}
		}
	}

	
	//Initialization of the desks in the GUI
	public void initDesks() {
		for (int i = 0; i < model.getDesks().length; i++) {
			view.setDeskText(i, "This desk is available.");
		}
	}
	
	
	//Main method, starts the simulation
	public void startCheckIn() {
		ArrayList<Booking> allPassenger = new ArrayList<Booking>();
		AirportClock airportClock = AirportClock.getInstance();
		Logging myLog = Logging.getInstance();
		
		// Initialise the desks and start the desk threads
		initDesks();
		model.start();
		
		for (Flight flight : model.getFlightMap().values()) {
			for (Booking booking : flight.getBookingList()) {
				allPassenger.add(booking);
			}
			view.addPlane(flight.getFlightNumber(), flight.getFlightInfo());
		}
		
		// Initialise the waiting list (passengers not yet in the queue) and starts the queue thread
		model.getQueue().fillQueue(allPassenger, view);
		
		//Sets the duration of the simulation
		airportClock.setDurationInMillis(80000);
		
		while (!airportClock.isFinished()) {
			checkDesk();
			checkCurrentPassengers();
			checkFlights();
		}
		
		//Writing logs report and exiting
		System.out.println("Airport is closed!");
		Logging.writeLogFile();
		System.exit(0);
	}
}
	
