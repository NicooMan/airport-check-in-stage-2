package main;

import java.time.Instant;
import java.time.Duration;

/**
 * The main clock which will help us to know when to stop the program
 *
 */
public class AirportClock {
	private static double factorTime=1;
	private Instant start;
	private static long durationInMillis;
	private static AirportClock airportClock = new AirportClock();
	
	/**
	 * Creates the clock and set the starting point
	 */
	private AirportClock() {
		start = Instant.now();
	};
	
	/** get the factor time
	 * @return the factorTime
	 */
	public static double getFactorTime() {
		return factorTime;
	}

	/** set the factor time
	 * @param factorTime the factorTime to set
	 */
	public static void setFactorTime(double factorTime) {
		AirportClock.factorTime = factorTime;
	}
	/** Will make the time factor twice faster 
	 */
	public static void makeTimeFaster() {
		factorTime=factorTime/2;
		durationInMillis /= 2;
	}
	/** Will make the time factor twice slower
	 */
	public static void makeTimeSlower() {
		factorTime=factorTime*2;
		durationInMillis *= 2;
	}
	public static AirportClock getInstance() {
		return airportClock;
	}
	
	protected Instant getStartTime() {
		return start;
	}
	
	protected long getDurationInMillis() {
		return durationInMillis;
	}
	
	protected float getDurationInSeconds() {
		return durationInMillis / 1000;
	}
	
	protected void setDurationInMillis(long newDuration) {
		durationInMillis = newDuration;
	}
	
	protected void setDurationInSeconds(long newDuration) {
		durationInMillis = newDuration * 1000;
	}
	
	/**
	 * Sends true when the program has passed a certain duration
	 * @return boolean saying if the program must be finished or not
	 */
	protected boolean isFinished() {
		Duration timeElapsed = Duration.between(start, Instant.now());
		if (timeElapsed.toMillis() > durationInMillis) {
			return true;
		}
		return false;
	}
	
	protected Instant now() {
		return Instant.now();
	}
}
