package main;
import java.util.ArrayList;

public class Flight {

	//instance of all variable
	private String flightNumber;
	private String carieer;
	private String destination;
	private int maxPassenger;
	private double maxWeight;
	private int maxVolume;
	private ArrayList<Booking> bookingList;
	private double actualWeight=0;
	private int nbPassengerChecked=0;
	private double actualVolume=0;


	/** Create a new flight using all info
	 * @param flightNumber
	 * @param carieer
	 * @param destination
	 * @param maxPassenger
	 * @param maxWeight
	 * @param maxVolume
	 */
	public Flight(String flightNumber, String carieer, String destination, int maxPassenger, double maxWeight,
			int maxVolume) {
		this.flightNumber = flightNumber;
		this.carieer = carieer;
		this.destination = destination;
		this.maxPassenger = maxPassenger;
		this.maxWeight = maxWeight;
		this.maxVolume = maxVolume;
		this.bookingList = new ArrayList<Booking>();
	}
	
	/** get the flight number
	 * @return the flightNumber
	 */
	public String getFlightNumber() {
		return flightNumber;
	}
	/** set the flight number
	 * @param flightNumber the flightNumber to set
	 */
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}
	/** get the carieer of the flight
	 * @return the carieer
	 */
	public String getCarieer() {
		return carieer;
	}
	/** Set the Carieer of a flight
	 * @param carieer the carieer to set
	 */
	public void setCarieer(String carieer) {
		this.carieer = carieer;
	}
	/**Get the destination of the flight
	 * @return the destination
	 */
	public String getDestination() {
		return destination;
	}
	/** set the destination of a plane
	 * @param destination the destination to set
	 */
	public void setDestination(String destination) {
		this.destination = destination;
	}
	/** get the max passenger a plane can take
	 * @return the maxPassenger
	 */
	public int getMaxPassenger() {
		return maxPassenger;
	}
	/** Set the max passenger a plane can get
	 * @param maxPassenger the maxPassenger to set
	 */
	public void setMaxPassenger(int maxPassenger) {
		this.maxPassenger = maxPassenger;
	}
	/**return the max weight of a plane
	 * @return the maxWeight
	 */
	public double getMaxWeight() {
		return maxWeight;
	}
	/** Set the max Weight of a plane 
	 * @param maxWeight the maxWeight to set
	 */
	public void setMaxWeight(double maxWeight) {
		this.maxWeight = maxWeight;
	}
	/**Return the max volume of a flight
	 * @return the maxVolume
	 */
	public int getMaxVolume() {
		return maxVolume;
	}
	/**set MaxVolume of a flight
	 * @param maxVolume the maxVolume to set
	 */
	public void setMaxVolume(int maxVolume) {
		this.maxVolume = maxVolume;
	}
	/**Return an array with all the booking list
	 * @return the bookingList
	 */
	public ArrayList<Booking> getBookingList() {
		return bookingList;
	}
	/**
	 * @param bookingList the bookingList to set
	 */
	public void setBookingList(ArrayList<Booking> bookingList) {
		this.bookingList = bookingList;
	}
	
	/**
	 * @return the nbPassengerChecked
	 */
	public int getNbPassengerChecked() {
		return nbPassengerChecked;
	}

	public boolean doesPassengerExist(String lastName, String bookingCode) {
		for (Booking booking : bookingList) {
			if (booking.getName().getLastName() == lastName && booking.getBookingReference() == bookingCode) {
				return true;
			}
		}
		return false;
	}
	/**Function that will check the passenger given the booking.
	 * Will add the weight/volume of the baggage in the plane
	 */
	public void passengerBooked (Booking passBooking) {
		this.actualWeight=this.actualWeight+passBooking.getBaggage().getWeight();
		this.actualVolume=this.actualVolume+passBooking.getBaggage().getVolume();
		this.nbPassengerChecked=this.nbPassengerChecked+1;
	}
	/**Return an the actual weight of the plane
	 * @return the actualWeight
	 */
	public double getActualWeight() {
		return actualWeight;
	}
	public double getRemainingWeight() {
		return maxWeight-actualWeight;
	}
	/**Return an the actual volume of the plane
	 * @return the actualVolume
	 */
	public double getActualVolume() {
		return actualVolume;
	}
	/**Return an the remaining volume of the plane
	 * @return the remainingVolume
	 */
	public double getRemainingVolume() {
		return maxVolume-actualVolume;
	}
	/**Will give in % the hold in the plane. It will give either the volume or the weight
	 * @return a double the % of the plane hold
	 */
	public double getHold() {
		double hold = actualWeight/maxWeight;
		if (hold>(actualVolume/maxVolume)) {
			return Math.round(hold*100);
		}
		else{
			return Math.round(100*actualVolume/maxVolume);
		}
		
	}
	
	/**Get a flight info
	 * @return a string 
	 */
	public String getFlightInfo() {
		return getNbPassengerChecked() + " check in \n" +"out of "+ bookingList.size() +"\n" +"Hold is "+ getHold() + "% full";
	}
	/** Increment the number of passenger checked
	 */
	public void incrementPassengerChecked() {
		nbPassengerChecked += 1;
	}
	
	public String toString() {
		return "Carieer = " + carieer + ", destination = " + destination + ", maxPassenger = " + maxPassenger + ", maxWeight = " + maxWeight +
				", maxVolume = " + maxVolume; 
	}

}
