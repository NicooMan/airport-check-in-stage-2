package main;
/**
 * Class made to read booking and flight files and to write the report file
 * 
 * @author Jean-Alexandre
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;

public class IOHandler {
		private HashMap<String, Flight> flightMap;
		
		/**
		 * Creates a IOHandler object
		 */
		public IOHandler() {
			flightMap = new HashMap<String, Flight>();
		}
		
		/**
		 * Returns the flight HashMap
		 * @return the flight map
		 */
		public HashMap<String, Flight> getFlightMap() {
			return flightMap;
		}

		/**
		 * Sets the flight map instance
		 * @param flightMap the new flightMap
		 */
		public void setFlightMap(HashMap<String, Flight> flightMap) {
			this.flightMap = flightMap;
		}
		
		public void readAirlineFiles() {
			readFlight();
			readBooking();
		}
		/**
		 * Reads the flight file and fill the flight hashMap
		 */
		private void readFlight() {
			InputStream in = getClass().getResourceAsStream("/inputFiles/flight.csv");
    		if (in == null) {
    			System.out.println("FLIGHT NULL");
    		}
			Scanner scanner;
			boolean start = true;
			int lineNb = 1;
			
			scanner = new Scanner(in);
			while (scanner.hasNextLine()) {
				try {
					String flightLine = scanner.nextLine();
					
					if (!start) {
						checkFlightLine(flightLine, lineNb);
						lineNb += 1;
					}
					start = false;
				} catch (IllegalStateException e){
					System.out.println(e.getMessage());
				}
			}
			scanner.close();
		}
		
		/**
		 * Check if line is perfectly formatted and if true, creates a new element in the flightMap HashMap
		 * @param line the line read
		 * @param lineNb the line number to know where the error happens
		 * @throws IllegalStateException error sent when the line has a format error
		 */
		private void checkFlightLine(String line, int lineNb) throws IllegalStateException {
			if (line.charAt(line.length()-1) == '\n') {
				line = line.substring(0, line.length() - 1);
			}
			String[] lineArray = line.trim().split(";");
			String flightCodePattern = "[A-Z]{2}\\d{4}";
			
			if (lineArray.length != 6) {
				
				throw new IllegalStateException("The line " + lineNb + " doesn't have the right amount of information.");
			}
			if (!lineArray[0].matches(flightCodePattern)) {
				throw new IllegalStateException("Wrong flight code format at line " + lineNb + ".");
			}
			if (!lineArray[1].matches("[A-Za-z ]+")) {
				throw new IllegalStateException("City name should only contain letters at line " + lineNb + ".");
			}
			if (!lineArray[3].matches("\\d+")) {
				throw new IllegalStateException("The max number of passengers must contain digits only at line " + lineNb + ".");
			}
			if (!lineArray[4].matches("\\d+.?\\d+")) {
				throw new IllegalStateException("The max baggage weight must contain only digits at and/or a dot line " + lineNb + ".");
			}
			if (!lineArray[5].matches("\\d+")) {
				throw new IllegalStateException("The max volume of the flight must contain only digits at line " + lineNb + ".");
			}
			try {
				flightMap.put(lineArray[0], new Flight(lineArray[0], lineArray[2], lineArray[1], Integer.parseInt(lineArray[3]),
						Double.parseDouble(lineArray[4]), Integer.parseInt(lineArray[5])));
			} catch (NumberFormatException e) {
				throw new IllegalStateException("Max passenger or max baggage weight or max flight volume contains forbidden characters at line " + lineNb + ".");
			}
		}
		
		private void readBooking(){
	    	try {
	    		InputStream in = IOHandler.class.getResourceAsStream("/inputFiles/booking_list.csv");
	    		if (in == null) {
	    			System.out.println("BOOKING LIST NULL");
	    		}
				Scanner scanner = new Scanner(in);
				while (scanner.hasNextLine()) {
					// read first line and process it
					String inputLine = scanner.nextLine();
					if (inputLine.length() != 0) {// ignored if blank line
						try {
							processLineBooking(inputLine);
						} 
						catch (StringIndexOutOfBoundsException siobe) {
							System.out.println("Error in the data! Please enter correct data!");
						}
					}
				}
				scanner.close();
			}

			catch (StringIndexOutOfBoundsException siobe) {
				System.out.println("Error in the archer data! Please enter correct data!");
				// System.exit(0);
			}
			
		}
		
		private void processLineBooking(String inputLine) {
			//Split the ligne by ;
			String separated[] = inputLine.split(";");
			// check if the flight in our flight list
			if (flightMap.get(separated[2])!=null) {
				String bookingCodePattern = "[A-Z]{1}\\d{3}[A-Z]{1}\\d{1}";
				if (!separated[0].matches(bookingCodePattern)) {
					throw new IllegalStateException("Wrong booking code format at line " + inputLine + ".");
				}
				if (!separated[1].matches("[A-Za-z- ]+")) {
					throw new IllegalStateException("Name should only contains letters at line " + inputLine + ".");
				}
				Name nameBooking = new Name (separated[1]);
				boolean checkedBooking = Boolean.parseBoolean(separated[3]);
				Booking bookedBooking = new Booking(nameBooking,separated[0],checkedBooking);
				flightMap.get(separated[2]).getBookingList().add(bookedBooking);
			}
			else {
				// line ignored
			}	
		}

}
