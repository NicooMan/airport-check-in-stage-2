package main;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 * The logging class, saving all entries and writing them in a file at the end of the program
 *
 */
public class Logging {
	private static Logging log = new Logging();
	private static ArrayList<String> logEntries = new ArrayList<String>();
	
	private Logging() {
		
	}
	
	public static Logging getInstance() {
		return log;
	}
	
	/**
	 * add an entry in the ArrayList log
	 * @param entry a string 
	 */
	public static void addEntry(String entry) {
		synchronized(logEntries) {
			logEntries.add(entry);
		}
	}
	
	/**
	 * Write all the entries in the "report.txt" file
	 */
	public static void writeLogFile() {
		FileWriter fw;
		
		try {
			fw = new FileWriter("./report.txt");
			System.out.println("Writing the report in the file \"report.txt\"...");
			for (String entry: logEntries) {
				fw.write(entry + "\n");
			}
			fw.close();
			System.out.println("Report written!");
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}
}
