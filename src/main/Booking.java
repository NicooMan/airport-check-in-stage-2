package main;

public class Booking {
	
	private Name name;
	private String bookingReference;
	private boolean checked;
	private Baggage baggage;


	public Booking(Name name, String BookingReference,boolean Checked) {
		this.name=name;
		this.bookingReference=BookingReference;
		this.checked=Checked;
		this.baggage=new Baggage();
		baggage.setBagageRandom();
		baggage.calculatePrice();
	}
	
	/**
	 * @return the baggage
	 */
	public Baggage getBaggage() {
		return baggage;
	}
	/**
	 * @param set the baggage
	 */
	public void setBaggage(Baggage baggage) {
		this.baggage = baggage;
	}
	/**
	 * @return name
	 */
	public Name getName() {
		return name;
	}
	/**
	 * @return the booking reference number
	 */
	public String getBookingReference() {
		return bookingReference;
	}
	/**
	 * @return a boolean to know if it has been checked
	 */
	public boolean isChecked() {
		return checked;
	}
		
	public void setName (Name name) {
		this.name = name;
	}
	public void setBookingReference( String BookingReference) {
		this.bookingReference=BookingReference;
	}
	public void setChecked( boolean Checked) {
		this.checked=Checked;
	}
	public String toString() {
		return name.getFirstAndLastName() + " " +  bookingReference + " " + ((checked) ? "true" : "false");
	}
	
	public String getBookingInfo() {
		// if the passenger is already checked in, it means he went to the desk only to put his baggage in the hold
		return (checked ? 
				name.getFirstAndLastName() + "\n"
				+ "is dropping off\n"
				+ "1 bag of " + baggage.getWeight() + " kg.\n"
				+ "He already checked in\n"
				+ "with a fee of £" + baggage.getPrice() + "."
				:
				name.getFirstAndLastName() + "\n"
				+ "is dropping off\n"
				+ "1 bag of " + baggage.getWeight() + "kg.\n"
				+ "A baggage fee of\n"
				+ "£" + baggage.getPrice() + " is due.");
	}
}
