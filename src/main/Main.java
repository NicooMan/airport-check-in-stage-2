package main;

import java.util.HashMap;

public class Main {

	public static void main(String[] args) {

		GUI view = new GUI();
		view.pack();
		view.setVisible(true);

		IOHandler myIOHandler = new IOHandler();
		myIOHandler.readAirlineFiles();
		HashMap<String, Flight> flightMap = myIOHandler.getFlightMap();
		
		Airport model = new Airport(flightMap);

		Controller controller = new Controller(view, model);
		
		controller.startCheckIn();
	}

}
