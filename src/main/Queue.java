package main;

import java.util.ArrayList;



import java.util.LinkedList;
import java.util.Random;


/**
 * Class representing the queue
 *
 * Extends Thread so it runs in parallel with the desks
 */
public class Queue extends Thread {
	
	//List of passengers in the queue
	private LinkedList<Booking> queue;
	
	//List of passengers not yet in the queue (waiting list)
	private ArrayList<Booking> allPassenger;
	
	private GUI view;
	
	public Queue() {
		queue = new LinkedList<Booking>();
		allPassenger = null;
		view = null;
	}
	
	//Add a passenger at the end of the queue (LinkedList method)
	public void addToQueue(Booking booking) {
		queue.addLast(booking);
	}
	
	//Gets the first person in the queue to process it
	public Booking getAndRemoveFromQueue() {
		Booking booking = queue.getFirst();
		queue.removeFirst();
		return booking;
	}
	
	public LinkedList<Booking> getLinkedList() {
		return queue;
	}
	
	//Fill the waiting list and start the queue thread
	public void fillQueue(ArrayList<Booking> allPassenger, GUI view) {
		this.allPassenger = allPassenger;
		this.view = view;
		this.start();
	}
	
	//Actions when the thread is started
	public void run() {
		Random r = new Random();
		while (!allPassenger.isEmpty()) {
			synchronized(queue) {
				int randomIndex = r.nextInt((allPassenger.size() - 1 - 0) + 1);
				Booking newPassengerInQueue = allPassenger.get(randomIndex);
				addToQueue(newPassengerInQueue);
				view.add(newPassengerInQueue);
				allPassenger.remove(randomIndex);
				Logging.addEntry("Passenger " + newPassengerInQueue.getBookingReference() + " (" + newPassengerInQueue.getName().getFirstAndLastName() + ") entered the queue.");
			}
			try {
				Thread.sleep((long) (AirportClock.getFactorTime() * 1000 * (Math.random() + 0.5)));
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
