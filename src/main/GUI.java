package main;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.HashMap;
import java.util.Hashtable;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 * The Class GUI.
 */
public class GUI extends JFrame implements ActionListener, ChangeListener {

	// Creating fields for the GUI
	private JTextField queue;
	private JTextField speed;
	private JTextField desk;
	private JTextField plane;
	private JList<Booking> list;
	private DefaultListModel<Booking> listModel;
	private HashMap<String, JTextArea[]> planeLane = new HashMap<String, JTextArea[]>();
	private JTextArea[] deskArray = new JTextArea[5];
	private JTextArea[] textDeskArray = new JTextArea[5];
	private JButton[] buttonDesk = new JButton[5];
	private ButtonGroup groupButton = new ButtonGroup();
	private Container pane = this.getContentPane();
	private GridBagConstraints c = new GridBagConstraints();
	private GridBagLayout layout = new GridBagLayout();
	private boolean[] boolButton = new boolean[5];

	// Create the constant values fort the speed meter
	static final int INTITAL_SPEED = 0;
	static final int MAX_SPEED = 2;
	static final int MIN_SPEED = -2;
	private int previousValue = 0;

	// Initial value for the number of peoples waiting
	private int x = 0;

	// Color of the borders of the GUI
	Border blackline = BorderFactory.createLineBorder(Color.LIGHT_GRAY);

	/**
	 * Instantiates a new GUII.
	 *
	 * @throws HeadlessException the headless exception
	 */
	public GUI() throws HeadlessException {

		pane.setLayout(layout);
		this.setTitle("Airport Check-In");
		this.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent evt) {
				endProgram();
			}
		});
		this.setExtendedState(JFrame.NORMAL);
		this.setPreferredSize(new Dimension(800, 700));

		// Creating text fields
		queue = new JTextField();
		queue.setText("There is currently " + x + " person waiting in the queue");
		queue.setEditable(false);
		queue.setBorder(BorderFactory.createCompoundBorder(blackline, BorderFactory.createEmptyBorder(5, 5, 5, 5)));
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 1;
		c.gridwidth = 5;
		c.gridx = 0;
		c.gridy = 0;
		pane.add(queue, c);

		desk = new JTextField();
		desk.setText("Check-In desks");
		desk.setHorizontalAlignment(JTextField.CENTER);
		desk.setEditable(false);
		desk.setBorder(BorderFactory.createCompoundBorder(blackline, BorderFactory.createEmptyBorder(5, 5, 5, 5)));
		c.weightx = 0.1;
		c.gridwidth = 5;
		c.gridx = 0;
		c.gridy = 2;
		pane.add(desk, c);

		plane = new JTextField();
		plane.setText("Planes");
		plane.setHorizontalAlignment(JTextField.CENTER);
		plane.setEditable(false);
		plane.setBorder(BorderFactory.createCompoundBorder(blackline, BorderFactory.createEmptyBorder(5, 5, 5, 5)));
		c.weightx = 0.1;
		c.gridwidth = 5;
		c.gridx = 0;
		c.gridy = 7;
		pane.add(plane, c);

		speed = new JTextField();
		speed.setText("Speed meter");
		speed.setHorizontalAlignment(JTextField.CENTER);
		speed.setEditable(false);
		speed.setBorder(javax.swing.BorderFactory.createEmptyBorder()); // delete borders
		c.weightx = 0.33;
		c.gridwidth = 5;
		c.gridx = 0;
		c.gridy = 12;
		pane.add(speed, c);

		// Create a JList for Booking so it shows a scroll-able list
		listModel = new DefaultListModel<Booking>();
		list = new JList<Booking>(listModel);
		list.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
		list.setLayoutOrientation(JList.VERTICAL);
		list.setVisibleRowCount(-1); // All rows are visible

		// Creation of the ScrollPane for the list of booking names
		JScrollPane listScroller = new JScrollPane(list);
		listScroller.setPreferredSize(new Dimension(600, 300));
		c.fill = GridBagConstraints.BOTH;
		c.gridwidth = 5;
		c.gridx = 0;
		c.gridy = 1;
		c.weightx = 1;
		c.weighty = 1;
		pane.add(listScroller, c);
		pane.setVisible(true);

		// Create the desks with the on/off button
		for (int i = 0; i < 5; i++) {
			// The desk name
			deskArray[i] = new JTextArea();
			deskArray[i].setText("Desk" + (i + 1));
			deskArray[i].setEditable(false);
			deskArray[i].setBorder(
					BorderFactory.createCompoundBorder(blackline, BorderFactory.createEmptyBorder(5, 5, 5, 5)));
			c.fill = GridBagConstraints.BOTH;
			c.weighty = 0;
			c.weightx = 0.33;
			c.gridwidth = 1;
			c.gridx = i;
			c.gridy = 3;
			pane.add(deskArray[i], c);

			// The desk displaying area
			textDeskArray[i] = new JTextArea();
			textDeskArray[i].setEditable(false);
			textDeskArray[i].setBorder(
					BorderFactory.createCompoundBorder(blackline, BorderFactory.createEmptyBorder(5, 5, 5, 5)));
			c.fill = GridBagConstraints.BOTH;
			c.weightx = 0.33;
			c.gridwidth = 1;
			c.gridx = i;
			c.gridy = 4;
			pane.add(textDeskArray[i], c);

			// The on/off buttons
			String inter = "On";
			buttonDesk[i] = new JButton(inter);
			buttonDesk[i].addActionListener(this);
			buttonDesk[i].setBackground(Color.green);
			buttonDesk[i].setOpaque(true);
			buttonDesk[i].setBorder(
					BorderFactory.createCompoundBorder(blackline, BorderFactory.createEmptyBorder(5, 5, 5, 5)));
			c.fill = GridBagConstraints.BOTH;
			c.weightx = 0.33;
			c.gridwidth = 1;
			c.gridx = i;
			c.gridy = 5;
			String buttonname = Integer.toString(i);
			buttonDesk[i].setName(buttonname);
			boolButton[i] = true;
			groupButton.add(buttonDesk[i]);
			pane.add(buttonDesk[i], c);
		}

		// Creation of the speed slider
		JSlider speed = new JSlider(JSlider.HORIZONTAL, MIN_SPEED, MAX_SPEED, INTITAL_SPEED);
		speed.addChangeListener(this);
		// Turn on labels at major tick marks.
		speed.setMajorTickSpacing(1);
		speed.setMinorTickSpacing(1);
		speed.setPaintTicks(true);

		// Create the label table
		Hashtable<Integer, JLabel> labelTable = new Hashtable<Integer, JLabel>();
		labelTable.put(MAX_SPEED, new JLabel("x4"));
		labelTable.put(1, new JLabel("x2"));
		labelTable.put(0, new JLabel("x1"));
		labelTable.put(-1, new JLabel("x0.5"));
		labelTable.put(MIN_SPEED, new JLabel("x0.25"));
		speed.setLabelTable(labelTable);
		speed.setPaintLabels(true);
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridwidth = 5;
		c.gridx = 0;
		c.gridy = 13;
		pane.add(speed, c);

	}

	/**
	 * Removes the first person of the list.
	 */
	public void removeFirst() {
		listModel.remove(0);
		queue.setText("There are currently " + listModel.getSize() + " people waiting in the queue");
	}

	/**
	 * Adds a person to the end of the list
	 *
	 * @param personBooking the person booking
	 */
	public void add(Booking personBooking) {
		listModel.addElement(personBooking);
		queue.setText("There are currently " + listModel.getSize() + " people waiting in the queue");

	}

	/**
	 * Adds a plane
	 *
	 * @param planeName the plane name
	 * @param planeText the plane informations
	 */
	public void addPlane(String planeName, String planeText) {

		// Add the plane if it does not already exist
		if (planeLane.containsKey(planeName) == false) {
			// Create the field for the plane name
			planeLane.put(planeName, new JTextArea[2]);
			planeLane.get(planeName)[0] = new JTextArea();
			planeLane.get(planeName)[0].setEditable(false);
			planeLane.get(planeName)[0].setText(planeName);
			planeLane.get(planeName)[0].setBorder(
					BorderFactory.createCompoundBorder(blackline, BorderFactory.createEmptyBorder(5, 5, 5, 5)));
			c.fill = GridBagConstraints.HORIZONTAL;
			c.weightx = 0.33;
			c.gridwidth = 1;
			c.gridx = (planeLane.size() - 1) % 5; // Place the planes on the GUI by lines of 5
			c.gridy = 8 + ((planeLane.size() - 1) / 5) * 2; // go at next line after the 5 first planes
			pane.add(planeLane.get(planeName)[0], c);

			// Create the field for the plane text display area
			planeLane.get(planeName)[1] = new JTextArea();
			planeLane.get(planeName)[1].setEditable(false);
			planeLane.get(planeName)[1].setText(planeText);
			planeLane.get(planeName)[1].setBorder(
					BorderFactory.createCompoundBorder(blackline, BorderFactory.createEmptyBorder(5, 5, 5, 5)));
			c.fill = GridBagConstraints.HORIZONTAL;
			c.weightx = 0.33;
			c.gridwidth = 1;
			c.gridy = 9 + ((planeLane.size() - 1) / 5) * 2;
			pane.add(planeLane.get(planeName)[1], c);
			revalidate();
		}
	}

	/**
	 * Sets the desk text.
	 *
	 * @param deskNumber the desk number
	 * @param deskText   the desk text to be displayed
	 */
	public void setDeskText(int deskNumber, String deskText) {
		textDeskArray[deskNumber].setText(deskText);
	}

	/**
	 * Sets the plane text.
	 *
	 * @param planeName the plane name
	 * @param planeText the plane text to be displayed
	 */
	public void setPlaneText(String planeName, String planeText) {
		planeLane.get(planeName)[1].setText(planeText);
	}

	/**
	 * Action performed.
	 *
	 * @param e the event
	 */
	public void actionPerformed(ActionEvent e) {
		JButton button = (JButton) e.getSource();
		int number = Integer.parseInt(button.getName());
		String onOff = button.getText();
		// Switching from on to off
		if (onOff == "On") {
			button.setText("Off");
			button.setBackground(Color.red);
			boolButton[number] = false;
		}
		// Switching from off to on
		if (onOff == "Off") {
			button.setText("On");
			button.setBackground(Color.green);
			boolButton[number] = true;
			setDeskText(number, "The desk is available.");
		}
	}

	/**
	 * Button boolean for on/off.
	 *
	 * @return the boolean[]
	 */
	public boolean[] buttonBoolean() {
		return boolButton;
	}

	/**
	 * End program, Ask IOHandler to write the Report.
	 */
	private void endProgram() {
		Logging.writeLogFile();
		System.exit(0);
	}

	/**
	 * State changed for the speed slider.
	 *
	 * @param e the event
	 */
	@Override
	public void stateChanged(ChangeEvent e) {

		JSlider source = (JSlider) e.getSource();
		// Adjust the time by calling AirportClock
		if (!source.getValueIsAdjusting()) {
			int newValue = (int) source.getValue() * 2;
			int gapValue = (newValue - previousValue) / 2;
			if (gapValue > 0) {
				while (gapValue != 0) {
					AirportClock.makeTimeFaster();
					gapValue = gapValue - 1;
				}
			} else if (gapValue < 0) {
				while (gapValue != 0) {
					AirportClock.makeTimeSlower();
					gapValue = gapValue + 1;
				}
			}
			previousValue = newValue; // set new value
		}
	}
}
