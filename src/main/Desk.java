package main;


import java.util.HashMap;

/**
 * Class representing a check in desk in the airport
 * Extends Thread to make the desks work in parallel
 */
public class Desk extends Thread {

    private HashMap<String, Flight> flightMap;
    private boolean status;
    private Booking currentBooking;
    private Queue queue;
    private int nb;
    private String currentFlight;
    private boolean needSleep;

    /**
     * Creates a new desk
     * @param nb a specific number representing the desk
     * @param flightMap the hashmap with all the flights and all the bookings
     * @param queue the queue of passengers in the airport
     */
    public Desk(int nb, HashMap<String, Flight> flightMap, Queue queue) {
    	this.flightMap = flightMap;
    	this.status = false;
    	this.currentBooking = null;
    	this.currentFlight = null;
    	this.queue = queue;
    	this.nb = nb;
    	this.needSleep = false;
    }

    /**
     * 
     * @return the flightMap (hashMap<String, Flight>)
     */
    public HashMap<String, Flight> getFlightMap(){
    	return flightMap;
    }
    
    /** 
     * 
     * @return the status (boolean)
     */
    public boolean getStatus() {
    	return status;
    }
    
    /**
     * 
     * @param bool the state of the desk (open or close)
     */
    public void setStatus(boolean bool) {
    	this.status = bool;
    }
    
    /**
     * 
     * @return the booking the desk is working on 
     */
    public Booking getCurrentBooking() {
    	return currentBooking;
    }
    
    /**
     * 
     * @param booking the booking the desk is working on
     */
    public void setCurrentBooking(Booking booking) {
    	this.currentBooking = booking;
    }
    
    /**
     * 
     * @return the id of the current booking's flight
     */
    public String getCurrentFlight() {
    	return currentFlight;
    }
    
    /**
     * 
     * @return the unique nb for the desk
     */
    public int getNb() {
    	return nb;
    }
    
	/** Check-in a passenger if he is not already checked-in and if his info and baggage are valid.
	 * @param lastName
	 * @param bookinRef
	 */
    public void check(String lastName, String bookingRef) {
    	Booking booking = findBooking(bookingRef);
    	Flight flight = findFlight(booking);
    	if (booking.isChecked()){
    		if (isValidName(lastName, bookingRef) && isValidBaggage(lastName, bookingRef)) {
    		Logging.addEntry("Passenger " + booking.getBookingReference() + " (" + booking.getName().getFirstAndLastName() + ") was already checked in.");
    		flight.passengerBooked(booking);
    		}
    		else {
    			Logging.addEntry("Passenger " + booking.getBookingReference() + " (" + booking.getName().getFirstAndLastName() + ") was already checked in but can't access the plane with his baggage, the plane is full.");
    		}
    	}
    	else if (!booking.isChecked()) {
    		if (isValidName(lastName, bookingRef) && isValidBaggage(lastName, bookingRef)) {
    			booking.setChecked(true);
    			flight.passengerBooked(booking);
    			Logging.addEntry("Passenger " + booking.getBookingReference() + " (" + booking.getName().getFirstAndLastName() + ") succesfully checked in for the flight " + flight.getFlightNumber() + " flying to " + flight.getDestination() + ".");
    		}
    		else {
    			Logging.addEntry("Passenger " + booking.getBookingReference() + " (" + booking.getName().getFirstAndLastName() + ") can't access the plane with his baggage, the plane is full.");
    		}
    	}
    }
    
    /**
     * Opens the desk by setting its status to true
     */
    public void open() {
    	this.setStatus(true);
    }
    
    /**
     * Closes the desk by setting its status to false
     */
    public void close() {
    	this.setStatus(false);
    }
    
    
	/** Check if the last name and booking ref match
	 * @param lastName
	 * @param bookinRef
	 * @return a boolean
	 */
    public boolean isValidName(String lastName, String bookingRef) {
    	Booking booking = findBooking(bookingRef);
    	if (booking != null) {
        	if (bookingRef.equals(booking.getBookingReference()) && lastName.toLowerCase().equals(booking.getName().getLastName().toLowerCase())) {
    			return true;
    		}
    	}
    	return false;
    }
    
	/** Check if the baggage corresponding to a booking fits in the plane, 
	 * i.e. if its weight and volume are lower than the remaining weight and volume available in the flight
	 * @param lastName
	 * @param bookinRef
	 * @return a boolean
	 */
    public boolean isValidBaggage(String lastName, String bookingRef){
    	Booking booking = findBooking(bookingRef);
    	Baggage baggage = booking.getBaggage();
    	Flight flight = findFlight(booking);
    	
    	int[] dim = baggage.getDimension();
    	double weight = baggage.getWeight();
    	int volume = dim[0]*dim[1]*dim[2];
    	if (volume < flight.getRemainingVolume() && (weight < flight.getRemainingWeight())) {
    		return true;
    	}
    	return false;
    }
    
	/** Find a booking
	 * @param bookingReference
	 * @return the booking
	 */
    public Booking findBooking(String bookingReference) {
    	for (Flight flight : flightMap.values()) {
    		for (Booking booking : flight.getBookingList()) {
    			if (booking.getBookingReference().equals(bookingReference)) {
    				return booking;
    			}
    		}
    	}
    	return null;
    }
    
	/** Find a flight
	 * @param booking
	 * @return the flight
	 */
    public Flight findFlight(Booking booking){
    	for (Flight flight : flightMap.values()) {
    		for (Booking b : flight.getBookingList()) {
    			if (b.getBookingReference().equals(booking.getBookingReference())) {
    				return flight;
    			}
    		}
    	}
    	return null;
    }
    
    /**
     * the actions of the thread when it's started
     */
    public void run() {
    	// always active until the end of program
    	while(true) {
    		// if desk is open
    		if(status == true) {
    			// protects the variable queue so it's changed by one thread at a time
    			synchronized(queue) {
    	    		if (queue.getLinkedList().size() > 0) {
    	    			// get the first passenger in queue to work on it
    		    		Booking current = queue.getAndRemoveFromQueue();
    		    		Logging.addEntry("Passenger " + current.getBookingReference() + " (" + current.getName().getFirstAndLastName() + ") joined the desk " + nb + ".");
    		    		// sets the current booking, flight and the needSleep variable so the threads knows a passenger is at the desk
    		    		this.currentBooking = current;
    		    		this.currentFlight = findFlight(current).getFlightNumber();
    		    		this.needSleep = true;
    	    		} else {
    	    			this.currentBooking = null;
    	    			this.currentFlight = null;
    	    			this.needSleep = false;
    	    		}
    	    	}
    			// simulates the passenger at the desk
    	    	if (needSleep) {
    		    	try {
    					Thread.sleep((long) (AirportClock.getFactorTime() * 10000 * (Math.random() + 0.5)));
    		    	} catch (InterruptedException e) {
    					e.printStackTrace();
    				}
    		    	// check the passenger in and change the flight accordingly
    		    	check(currentBooking.getName().getLastName(), currentBooking.getBookingReference());
    	    	}
    		} else {
    			try {
					Thread.sleep((long) (AirportClock.getFactorTime() * 1000));
		    	} catch (InterruptedException e) {
					e.printStackTrace();
				}
    		}
    	}
    }
}